import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Stack manipulation.
 *
 * @since 1.8
 */
public class DoubleStack {
   private LinkedList<Double> numbersInStack = new LinkedList<Double>();

   private enum TokenTypes {
      NUMBER,
      OPERATOR,
      WHITESPACETABCOMBO,
      SWAP,
      ROT,
      DUP
   }

   public static void main(String[] argum) {

        /*
        //Makes sure that an error is thrown when a unsupported operation is used.
        DoubleStack nn = new DoubleStack();
        nn.push(1);
        nn.push(1);
        nn.op("G");
        */

   }

   DoubleStack() {
   }

   DoubleStack(LinkedList setInStack) {
      numbersInStack = setInStack;
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack temp = new DoubleStack((LinkedList) this.numbersInStack.clone());
      return temp;
   }

   public boolean stEmpty() {
      return numbersInStack.isEmpty();
   }

   public void push(double a) {
      numbersInStack.push(a);
   }

   public double pop() {
      try {
         return numbersInStack.pop();
      } catch (NoSuchElementException e) {
         throw new NoSuchElementException("There were no more elements to pop()");
      }

   }

   public void op(String s) {

      try {
         double number1 = pop();
         double number2 = pop();

         switch (s) {
            case "+":
               push(number1 + number2);
               break;
            case "-":

               //substaaction explanation from web:
               //"the – operation: take the top two numbers off the stack, subtract the top one from the one below, and put the result back on the stack.
               //source:http://www-stone.ch.cam.ac.uk/documentation/rrf/rpn.html
               push(number2 - number1);
               break;
            case "*":
               push(number1 * number2);
               break;
            case "/":
               push(number2 / number1);
               break;
            default:
               throw new RuntimeException("The operand " + s + " was not recognized");
         }
      } catch (NoSuchElementException e) {
         throw new NoSuchElementException("Did not have enough elements to do the operation " + s);
      }

   }
   public void swap() {

      try {
         double number1 = pop();
         double number2 = pop();
         push(number1);
         push(number2);

         }
       catch (NoSuchElementException e) {
         throw new NoSuchElementException("Did not have enough elements to do the operation SWAP");
      }

   }
   public void rot() {

      try {
         double number1 = pop(); //c
         double number2 = pop(); //b
         double number3 = pop(); //a
         //"ROT roteerib kolme pealmist elementi, tõstes kolmanda esimeseks: a b c -- b c a "
         push(number2);
         push(number1);
         push(number3);

      }
      catch (NoSuchElementException e) {
         throw new NoSuchElementException("Did not have enough elements to do the operation ROT");
      }
   }
   public void dup() {

      try {
         double number1 = pop();
         push(number1);
         push(number1);
      }
      catch (NoSuchElementException e) {
         throw new NoSuchElementException("Did not have enough elements to do the operation DUP");
      }
   }

   public double tos() {
      if (numbersInStack.peek() != null) {
         return numbersInStack.peek();
      } else {
         throw new RuntimeException("Could not peek top of stack, because it was empty");
      }

   }

   @Override
   public boolean equals(Object o) {

      if (o instanceof DoubleStack) {
         //know we at least know, that we were given a DoubleStack
         DoubleStack temp = (DoubleStack) o;
         return numbersInStack.equals(temp.numbersInStack);

      } else {
         return false;
      }

   }

   @Override
   public String toString() {
      StringBuilder builder
              = new StringBuilder();
      Stack<Double> temp = new Stack<>();

      for (Double element : this.numbersInStack) {
         temp.push(element);
      }

      while (!temp.isEmpty()) {
         builder.append(temp.pop() + " ");
      }
      return builder.toString();
   }

   public static double interpret(String pol) {


      //We are going to have two major steps. First we will tokenize the input and then we will run the calculator
      //based on the tokens.

      if (pol == null) {
         throw new RuntimeException("The string that is interpreted can not be null");
      }
      if (pol.isEmpty()) {
         throw new RuntimeException("The string that is interpreted can not be empty");
      }

      //We are going to weed out some of the illegal inputs by regex.
      boolean ifNoIllegalSymbols = pol.matches("((-)?(\\d+[\\.]?)*((\\s)|(\\\\t))*|(\\+|\\*|\\/|-)*|SWAP|ROT|DUP)*");
      if (!ifNoIllegalSymbols) {
         throw new RuntimeException("The string contained illegal symbols or was not formed correctly (for example "
                 + "two dots after a number");
      }


      //This keeps the tokens.
      LinkedList<AbstractMap.SimpleEntry<TokenTypes, String>> thingsToDoFormated = new LinkedList<>();

      //this keeps track of text that has not yet been tokenized.
      String temp = String.valueOf(pol);


      while (temp.length() > 0) {

         // the regular expressions were created/tested in https://regexr.com/

         // this is the regex for operators
         Pattern pattern = Pattern.compile("^(\\+|\\*|\\/|-)");
         Matcher matcherOP = pattern.matcher(temp);

         //this is the regex for numbers
         Pattern pattern2 = Pattern.compile("(-)?(\\d+[\\.]?)");
         Matcher matcherNumber = pattern2.matcher(temp);

         //this is the regex for whitespace and tabs
         Pattern pattern3 = Pattern.compile("((\\s)|(\\\\t))+");
         Matcher matcherWhitespace = pattern3.matcher(temp);

         //this is the regex for SWAP
         Pattern pattern4 = Pattern.compile("SWAP");
         Matcher matcherSWAP = pattern4.matcher(temp);

         //this is the regex for ROT
         Pattern pattern5 = Pattern.compile("ROT");
         Matcher matcherROT = pattern5.matcher(temp);

         //this is the regex for ROT
         Pattern pattern6 = Pattern.compile("DUP");
         Matcher matcherDUP = pattern6.matcher(temp);

         if (matcherNumber.lookingAt()) {
            //a number was found
            // System.out.println("Found a number, it was: "+matcherNumber.group());

            //if there is a dot at the end, we want to remove it
            String numberback = matcherNumber.group();
            if ((numberback.substring(numberback.length() - 1)).equals(".")) {
               //then the last character in the string is a dot. So we have to remove it.
               thingsToDoFormated.add(new AbstractMap.SimpleEntry(TokenTypes.NUMBER, numberback.substring(0, numberback.length() - 1)));
            } else {
               //no dot at the end, just add as is.
               thingsToDoFormated.add(new AbstractMap.SimpleEntry(TokenTypes.NUMBER, matcherNumber.group()));
            }

            temp = temp.substring(matcherNumber.end());
         } else if (matcherSWAP.lookingAt()) {
            //a SWAP was found
            thingsToDoFormated.add(new AbstractMap.SimpleEntry(TokenTypes.SWAP, matcherSWAP.group()));
            temp = temp.substring(matcherSWAP.end());

         }
         else if (matcherROT.lookingAt()) {
            //a SWAP was found
            thingsToDoFormated.add(new AbstractMap.SimpleEntry(TokenTypes.ROT, matcherROT.group()));
            temp = temp.substring(matcherROT.end());

         }
         else if (matcherDUP.lookingAt()) {
            //a SWAP was found
            thingsToDoFormated.add(new AbstractMap.SimpleEntry(TokenTypes.DUP, matcherDUP.group()));
            temp = temp.substring(matcherDUP.end());

         }

         else if (matcherOP.lookingAt()) {
            //a operand was found
            //System.out.println("Found an operator, it was: "+matcherOP.group());
            thingsToDoFormated.add(new AbstractMap.SimpleEntry(TokenTypes.OPERATOR, matcherOP.group()));
            temp = temp.substring(matcherOP.end());

         } else if (matcherWhitespace.lookingAt()) {
            //a number was found
            //System.out.println("Whitespace was found, it was: "+matcherWhitespace.group());

            //We are not going to add the whitespace or tabs, because they will not be needed
            temp = temp.substring(matcherWhitespace.end());
         } else {
            throw new IllegalStateException("No token recognized in string. This should not happen " +
                    "Input text was: " + pol);
         }
      }
      //To print out the found tokens, uncomment the line bellow
      //System.out.println(thingsToDoFormated);


      DoubleStack calc = new DoubleStack();
      //Looping over the tokens
      for (AbstractMap.SimpleEntry<TokenTypes, String> thing : thingsToDoFormated) {
         if (thing.getKey().equals(TokenTypes.NUMBER)) {
            calc.push(Double.parseDouble(thing.getValue()));
         } else if (thing.getKey().equals(TokenTypes.OPERATOR)) {
            calc.op(thing.getValue());
         }
       else if (thing.getKey().equals(TokenTypes.SWAP)) {
         calc.swap();
      }
       else if (thing.getKey().equals(TokenTypes.ROT)) {
            calc.rot();
         }
       else if (thing.getKey().equals(TokenTypes.DUP)) {
            calc.dup();
         }
      }


      if (calc.numbersInStack.size() > 1) {
         throw new RuntimeException("Too many numbers left after doing the operations " +
                 "(only the result should be left). The string being parsed was " + pol);
      }

      return calc.tos();
   }

}